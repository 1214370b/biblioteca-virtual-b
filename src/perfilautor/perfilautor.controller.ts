import { Controller, Get, HttpStatus, Param, Res } from '@nestjs/common';
import { response } from 'express';
import { PerfilautorDto } from 'src/dto/perfilautor.dto';
import { PerfilautorService } from './perfilautor.service';

@Controller('autor')
export class PerfilautorController {
  constructor(private perfilautorService: PerfilautorService) {}

  @Get()
  getAll(@Res() response) {
    this.perfilautorService
      .getAll()
      .then((pautorList) => {
        response.status(HttpStatus.OK).json(pautorList);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de usuarios' });
      });
  }

  @Get(':id')
  get(@Res() response, @Param('id') id) {
    this.perfilautorService
      .get(id)
      .then((perfilid) => {
        response.status(HttpStatus.OK).json(perfilid);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de registro' });
      });
  }
}
