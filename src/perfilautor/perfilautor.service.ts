import {BadRequestException,Injectable,NotFoundException,} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Usuarios } from 'src/entities/usuarios.entity';
import { Repository } from 'typeorm';
import { PerfilautorDto } from 'src/dto/perfilautor.dto';

@Injectable()
export class PerfilautorService {
  constructor(
    @InjectRepository(Usuarios)
    private readonly perfilRepository: Repository<Usuarios>,
  ) {}

  async getAll() {
    return await this.perfilRepository.find({ where: { rol: 'autor' } });
  }

  async get(id: number): Promise<Usuarios> {
    if (!id) {
      throw new BadRequestException('el id debe ser mandado');
    }

    return await this.perfilRepository.findOne(id);
  }
}
