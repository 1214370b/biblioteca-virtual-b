import { Controller,Post,Get,Res,Body,HttpStatus } from '@nestjs/common';
import {CategoriasService} from './categorias.service'
import { CategoriaDto } from '../dto/categoria.dto'

@Controller('categorias')
export class CategoriasController {

    constructor(private categoriaService: CategoriasService){
    }

    //crear categorias
    @Post()
    create (@Body() catalogoDo: CategoriaDto, @Res() response){
        this.categoriaService.catalogoNuevo(catalogoDo).then( nuevocatalogo => {
            response.status(HttpStatus.CREATED).json(nuevocatalogo);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({nuevocatalogo: "error en tu registro"});
      });
    }

    //mostrar categorias
    @Get()
    getAll(@Res() response){
        this.categoriaService.getAll().then(catalogosList => {
            response.status(HttpStatus.OK).json(catalogosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de registro'});
        })
    }    
}
