import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Categorias } from '../entities/categorias.entity';
import { Material } from '../entities/material.entity';
import { Articulos } from '../entities/articulos.entity';
import { Repository } from 'typeorm';
import { CategoriaDto } from '../dto/categoria.dto'
import * as moment from 'moment';

@Injectable()
export class CategoriasService {
    constructor(
        @InjectRepository(Categorias)
        private readonly crearArticuloRepository: Repository<Categorias>,
        ){}

        //lista de catergorias 
        async getAll() {
            return await this.crearArticuloRepository.find();
        }

        //crear catergorias 
        async catalogoNuevo(registroNuevoCa: CategoriaDto): Promise<Categorias>{
        const nuevo = new Categorias();
        nuevo.nombre = registroNuevoCa.nombre;
        nuevo.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString()
        return this.crearArticuloRepository.save(nuevo);
        }
}
