import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EstadisticasDto } from 'src/dto/estadisticas.dto';
import { MapaDto } from 'src/dto/mapa.dto';
import { Articulos } from 'src/entities/articulos.entity';
import { Material } from 'src/entities/material.entity';
import { Usuarios } from 'src/entities/usuarios.entity';
import { Repository } from 'typeorm';

@Injectable()
export class EstadisticasService {
    constructor(
        @InjectRepository(Usuarios)
        private readonly perfilesRepository: Repository<Usuarios>,

        @InjectRepository(Articulos)
        private readonly articulosRepository: Repository<Articulos>,

        @InjectRepository(Material)
        private readonly materialRepository: Repository<Material>,
        ){}


        async getEstadisticasGenerales(){
            const est = new EstadisticasDto();
            est.usuarios = await this.perfilesRepository.count({where: {activo: 1}})
            est.articulos = await this.articulosRepository.count({where: {activo: 1}})
            est.material = await this.materialRepository.count()
            return est;
        }

        
        async getNacionesCount() {
            const paises = new MapaDto();
            
            //seria genial tener un ciclo para esto, pero no puedo iterar con propiedades de una clase
            paises.MX = await this.perfilesRepository.count({where: {nacionalidad: "MX"}});
            paises.AR = await this.perfilesRepository.count({where: {nacionalidad: "AR"}});
            paises.BR = await this.perfilesRepository.count({where: {nacionalidad: "BR"}});
            paises.ES = await this.perfilesRepository.count({where: {nacionalidad: "ES"}});
            paises.FR = await this.perfilesRepository.count({where: {nacionalidad: "FR"}});
            paises.CL = await this.perfilesRepository.count({where: {nacionalidad: "CL"}});
            paises.CO = await this.perfilesRepository.count({where: {nacionalidad: "CO"}});
            paises.US = await this.perfilesRepository.count({where: {nacionalidad: "US"}});
            paises.VE = await this.perfilesRepository.count({where: {nacionalidad: "VE"}});

           return paises;
        }
}
