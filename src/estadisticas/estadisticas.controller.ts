import { Controller, Get, HttpStatus, Res } from '@nestjs/common';
import { EstadisticasService } from './estadisticas.service';

@Controller('estadisticas')
export class EstadisticasController {
    constructor(private estadisticasService: EstadisticasService){}

    @Get("mapa")
    getNacionesCount(@Res() response){
        this.estadisticasService.getNacionesCount().then(numNaciones => {
            response.status(HttpStatus.OK).json(numNaciones);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de datos'});
        })
    }

    @Get()
    getTotalUsuarios(@Res() response){
        this.estadisticasService.getEstadisticasGenerales().then(numUsuarios => {
            response.status(HttpStatus.OK).json(numUsuarios);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de datos'});
        })
    }

}
