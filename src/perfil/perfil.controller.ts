import { Controller, Get, HttpStatus, Param, Res } from '@nestjs/common';
import { PerfilService } from './perfil.service';

@Controller('perfil')
export class PerfilController {
  constructor(private perfilService: PerfilService) {}

  @Get()
  getAll(@Res() response) {
    this.perfilService
      .getAll()
      .then((registrosList) => {
        response.status(HttpStatus.OK).json(registrosList);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de perfiles' });
      });
  }

  @Get(':id')
  get(@Res() response, @Param('id') id: number) {
    this.perfilService
      .get(id)
      .then((perfilid) => {
        response.status(HttpStatus.OK).json(perfilid);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion del perfil' });
      });
  }
}
