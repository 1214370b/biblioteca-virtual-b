import {  BadRequestException,Injectable,NotFoundException,} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Usuarios } from 'src/entities/usuarios.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PerfilService {
  constructor(
    @InjectRepository(Usuarios)
    private readonly perfilRepository: Repository<Usuarios>,
  ) {}

  async getAll() {
    return await this.perfilRepository.find({ where: { rol: 'ADMIN' } });
  }

  async get(id: number) {
    
    if(isNaN(id)) return "mensaje: id debe ser numero";

    const perfil = await this.perfilRepository.findOne(id);
    
    if(!perfil){
      return "mensaje: no se encontro el usuario"
    }
    else return perfil;
  }
}
