/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CorreoDto } from 'src/dto/Correo.dto';
import { RegisterDto } from 'src/dto/register';
import { Usuarios } from 'src/entities/usuarios.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RecContrService {
  constructor(
    @InjectRepository(Usuarios)
    private readonly contrasenaRepository: Repository<Usuarios>,
  ) {}

  async Register(registroNuevo: CorreoDto, idR) {
    const nuevo = new Usuarios();
    nuevo.contrasena = registroNuevo.contrasena;

  await this.contrasenaRepository.update(
        {id: idR},
        {
           contrasena: registroNuevo.contrasena
            
        }
    );
  }


}
