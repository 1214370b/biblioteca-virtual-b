import { Body, Controller, HttpStatus, Param, Post, Res } from '@nestjs/common';
import { CorreoDto } from 'src/dto/Correo.dto';
import { RecContrService } from './reccontr.service';

@Controller('RecuperarContrasena')
export class RecContrController {
  constructor(private reccontr: RecContrService) {}

  @Post(':id')
  create(@Body() registerDo: CorreoDto, @Res() response, @Param('id') id) {
    this.reccontr
      .Register(registerDo,id)
      .then((contr) => {
        response.status(HttpStatus.CREATED).json(contr);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ contr: 'error en tu registro' });
      });
  }
}
