/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res } from '@nestjs/common';
import { MaterialDto } from 'src/dto/material.dto';
import { GestionCatalogoService } from './gestion-catalogo.service';

@Controller('gestion-catalogo')
export class GestionCatalogoController {
  constructor(private gestionCatalogoService: GestionCatalogoService) {}

  @Get()
  getMaterial(@Res() response) {
    this.gestionCatalogoService
      .getMaterial()
      .then((listMaterial) => {
        response.status(HttpStatus.OK).json(listMaterial);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de datos' });
      });
  }

  @Post()
  subirMaterial(@Body() materialDto: MaterialDto, @Res() response) {
    this.gestionCatalogoService
      .subirMaterial(materialDto)
      .then((registro) => {
        response.status(HttpStatus.CREATED).json(registro);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ catalogo: 'error en el registro de material' });
      });
  }

  @Put(':id')
  modificarMaterial(
    @Param('id') id: number,
    @Body() materialDto: MaterialDto,
    @Res() response,
  ) {
    this.gestionCatalogoService
      .modificarMaterial(id, materialDto)
      .then((registro) => {
        response.status(HttpStatus.OK).json(registro);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ catalogo: 'error al modificar' });
      });
  }

  @Delete(':id')
  eliminarMaterial(@Param('id') id: number, @Res() response) {
    this.gestionCatalogoService
      .eliminarMaterial(id)
      .then((resp) => {
        response.status(HttpStatus.OK).json(resp);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ catalogo: 'error al eliminar' });
      });
  }
}
