/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { MaterialDto } from 'src/dto/material.dto';
import { Material } from 'src/entities/material.entity';
import { Repository, UpdateResult } from 'typeorm';
import { Categorias } from '../entities/categorias.entity'

@Injectable()
export class GestionCatalogoService {
  constructor(
    @InjectRepository(Material)
    private readonly materialRepository: Repository<Material>,
    @InjectRepository(Categorias)
    private readonly categoriasRepository: Repository<Categorias>,
  ) {}

  async getMaterial() {
    return await this.materialRepository.find({ where: { activo: 1 } });
  }

  // crear material didactico
  async subirMaterial(entradaMaterial: MaterialDto): Promise<Material> {
    const nuevoMaterial = new Material();
    const categoriaid = await this.categoriasRepository.findOne(entradaMaterial.categoriaid);

        nuevoMaterial.categoria = categoriaid
        nuevoMaterial.titulo = entradaMaterial.titulo;
        nuevoMaterial.autor_del_doc = entradaMaterial.autor_del_doc;
        nuevoMaterial.fecha = entradaMaterial.fecha;
        nuevoMaterial.archivo = entradaMaterial.archivo;
        nuevoMaterial.descripcion = entradaMaterial.descripcion;
        nuevoMaterial.etiquetas = entradaMaterial.etiquetas;
        nuevoMaterial.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString();
        return this.materialRepository.save(nuevoMaterial)
    }

  async modificarMaterial(
    idM: number,
    entradaMaterial: MaterialDto,
  ): Promise<UpdateResult> {
    const materialEditable = this.materialRepository.findOne({
      where: { id: idM },
    });

    if (!materialEditable) {
      throw new BadRequestException('material no encontrado');
    }

    const datos = {
      titulo: entradaMaterial.titulo,
      autor_del_doc: entradaMaterial.autor_del_doc,
      categoriaid: entradaMaterial.categoriaid,
      fecha: entradaMaterial.fecha,
      archivo: entradaMaterial.archivo,
      descripcion: entradaMaterial.descripcion,
      etiquetas: entradaMaterial.etiquetas,
    };

    return this.materialRepository.update({ id: idM }, datos);
  }

  async eliminarMaterial(idM: number): Promise<UpdateResult> {
    return this.materialRepository.update({ id: idM }, { activo: false });
  }
}
