import { RecContrService } from './RecuperarContraseña/reccontr.service';
import { RecContrController } from './RecuperarContraseña/reccontr.controller';
import { GestionUsuariosService } from './GestionUsuarios/gestionusuarios.service';
import { GestionUsuariosController } from './GestionUsuarios/gestionusuarios.controller';
import { PerfilautorController } from './perfilautor/perfilautor.controller';
import { PerfilautorService } from './perfilautor/perfilautor.service';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RegistrationController } from './registro/registro.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegistersService } from './registro/registro.service';
import { Usuarios } from './entities/usuarios.entity';
import { PerfilService } from './perfil/perfil.service';
import { PerfilController } from './perfil/perfil.controller';
import { ConfigModule } from '@nestjs/config';
import { EstadisticasService } from './estadisticas/estadisticas.service';
import { EstadisticasController } from './estadisticas/estadisticas.controller';
import { Articulos } from './entities/articulos.entity';
import { Material } from './entities/material.entity';
import { blogController } from './blog/blog/blog.controller';
import { BlogService } from './blog/blog/blog.service';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from './login/login.constants';
import { LoginController } from './login/login.controller';
import { LoginService } from './login/login.service';
import { GestionCatalogoService } from './gestion-catalogo/gestion-catalogo.service';
import { GestionCatalogoController } from './gestion-catalogo/gestion-catalogo.controller';
import { PerfilusuarioController } from './perfilusuario/perfilusuario.controller';
import { PerfilusuarioService } from './perfilusuario/perfilusuario.service';
import { GestionArticulosService } from './gestion-articulos/gestion-articulos.service';
import { GestionArticulosController } from './gestion-articulos/gestion-articulos.controller';
import { Mensajes } from './entities/mensajes.entity';
import { Categorias } from './entities/categorias.entity';
import { ArticuloController } from './articulos/articulos.controller';
import { CategoriasController } from './categorias/categorias.controller';
import { ArticuloService } from './articulos/articulos.service';
import { CategoriasService } from './categorias/categorias.service';
import { PeticionesEdicion } from './entities/peticiones-edicion.entity';
import { MaterialController } from './catalogo/catalogo.controller'
import { MaterialService } from './catalogo/catalogo.service'
import { MaterialGuardado } from './entities/material-guardado.entity';
import { ArticulosGuardados } from './entities/articulos-guardados.entity'
import { AyudaController } from './ayuda/ayuda.controller';
import { SolicitudesService } from './Solicitudes/solicitudes.service';
import { SolicitudesController } from './Solicitudes/solicitudes.controller';
import { PeticionesDeAutor } from './entities/peticiones-de-autor.entity';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Usuarios, 
      Articulos, 
      Material,
      Mensajes,
      Categorias,
      PeticionesEdicion,
      MaterialGuardado,
      PeticionesEdicion,
      PeticionesDeAutor,
      MaterialGuardado,
      ArticulosGuardados,
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '21600s' },
    }),
  ],

  controllers: [
    RecContrController,
    GestionUsuariosController,
    AppController,
    RegistrationController,
    PerfilController,
    EstadisticasController,
    GestionCatalogoController,
    blogController,
    PerfilautorController,
    LoginController,
    PerfilusuarioController,
    GestionArticulosController,
    ArticuloController,
    CategoriasController,
    MaterialController,
    AyudaController,
    SolicitudesController,
  ],
  providers: [
    RecContrService,
    GestionUsuariosService,
    AppService,
    RegistersService,
    PerfilService,
    EstadisticasService,
    PerfilautorService,
    GestionCatalogoService,
    BlogService,
    LoginService,
    PerfilusuarioService,
    GestionArticulosService,
    ArticuloService,
    CategoriasService,
    MaterialService,
    SolicitudesService,
  ],
})
export class AppModule {}
