/* eslint-disable prettier/prettier */
import { Controller, Get, Res, HttpStatus, Param, Redirect, Post, Body } from '@nestjs/common';
import { ArticulosDto } from 'src/dto/articulos.dto';
import { BlogService } from './blog.service';

@Controller('blog')
export class blogController {
  constructor(private blogService: BlogService) {}

  @Get()
  getAll(@Res() response) {
    this.blogService
      .getAll()
      .then((blogList: any) => {
        response.status(HttpStatus.OK).json(blogList);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la consulta del blog' });
      });
  }

  @Get('ListadoCategorias')
  getCategoria(@Res() response) {
    this.blogService
      .getCategoria()
      .then((categoriasList: any) => {
        response.status(HttpStatus.OK).json(categoriasList);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la consulta del blog' });
      });
  }

  @Get(':id')
  get(@Res() response, @Param('id') id) {
    this.blogService
      .get(id)
      .then((blogid) => {
        response.status(HttpStatus.OK).json(blogid);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de id' });
      });
  }

  
  @Post()
  nuevoarticulo(@Body() ArticulosDto: ArticulosDto, @Res() response) {
    this.blogService
      .nuevoarticulo(ArticulosDto)
      .then((registro) => {
        response.status(HttpStatus.CREATED).json(registro);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ catalogo: 'error en el registro de material' });
      });
  }


  
}
