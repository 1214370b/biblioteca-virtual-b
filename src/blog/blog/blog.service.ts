/* eslint-disable @typescript-eslint/no-unused-vars */
import { BadRequestException } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ArticulosDto } from 'src/dto/articulos.dto';
import { ArticulosGuardados } from 'src/entities/articulos-guardados.entity';
import { Articulos } from 'src/entities/articulos.entity';
import { Categorias } from 'src/entities/categorias.entity';
import { Repository } from 'typeorm';
import * as moment from 'moment';

@Injectable()
export class BlogService {

  constructor(
    @InjectRepository(Articulos)
    private readonly articulosRepository: Repository<Articulos>,
  ) {}

  @InjectRepository(Categorias)
  private readonly CategoriasRepository: Repository<Categorias>;

  async getAll() {
    return await this.articulosRepository.find();
  }

  async getCategoria() {
    return await this.CategoriasRepository.find();
  }

  async get(id: number): Promise<Articulos> {
    if (!id) {
      throw new BadRequestException('el id debe ser mandado');
    }

    return await this.articulosRepository.findOne(id);
  }
  // eslint-disable-next-line @typescript-eslint/adjacent-overload-signatures
  async nuevoarticulo(
    nuevoarticulos: ArticulosDto,
  ): Promise<ArticulosGuardados> {
    const nuevoarticulo = new ArticulosGuardados();

    return this.articulosRepository.save(nuevoarticulo);
  }
}
