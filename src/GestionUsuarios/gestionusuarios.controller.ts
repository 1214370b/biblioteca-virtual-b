/* eslint-disable prettier/prettier */
import { Controller, Get, HttpStatus, Param, Res } from '@nestjs/common';
import { GestionUsuariosService } from './gestionusuarios.service';

@Controller('Usuarios')
export class GestionUsuariosController {
  constructor(private gestionUsuariosService: GestionUsuariosService) {}

  @Get()
  getall(@Res() response) {
    this.gestionUsuariosService
      .getUsuarios()
      .then((listUsuarios) => {
        response.status(HttpStatus.OK).json(listUsuarios);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de Usuarios' });
      });
  } 

  @Get(':id')
  get(@Res() response, @Param('id') id) {
    this.gestionUsuariosService
      .get(id)
      .then((usuarioid) => {
        response.status(HttpStatus.OK).json(usuarioid);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de id' });
      });
  }
 
}
