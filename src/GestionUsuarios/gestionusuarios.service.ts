/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Usuarios } from 'src/entities/usuarios.entity';
import { Repository } from 'typeorm';

@Injectable()
export class GestionUsuariosService {
    constructor(
    @InjectRepository(Usuarios)
    private readonly UsuariosRepository: Repository<Usuarios>,
  ) {}

  async getUsuarios() {
    return await this.UsuariosRepository.find({ where: { activo: 1 } });
  }

   async get(id: number): Promise<Usuarios> {
    if (!id) {
      throw new BadRequestException('el id debe ser mandado');
    }

    return await this.UsuariosRepository.findOne(id);
  }

}
