export class MaterialGuardadoDto {
    readonly usuarioid: number;
    readonly materialid: number;
    readonly fecha_de_registro: string;
}