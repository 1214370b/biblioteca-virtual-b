import { Rol } from "src/entities/usuarios.entity";

//Registro de usuarios 
export class RegisterDto {
    readonly Nombre: string;
    readonly Apellido: string;
    readonly nacionalidad: string;
    readonly contrasena: string;
    readonly fecha_de_nacimiento: string;
    readonly correo: string;
    readonly rol: Rol;
}