import { Usuarios } from "src/entities/usuarios.entity";

export class MensajeDto {
    readonly receptorId: number;
    readonly emisorId: number;
    readonly cuerpo: string;
}
