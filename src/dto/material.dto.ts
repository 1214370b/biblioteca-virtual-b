export class MaterialDto {
    readonly titulo: string;
    readonly autor_del_doc: string;
    readonly categoriaid: number;
    readonly fecha: string;
    readonly archivo: string;
    readonly descripcion: string;
    readonly etiquetas: string;
}
