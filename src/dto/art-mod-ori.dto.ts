import { Articulos } from "src/entities/articulos.entity";

export class ArtModOriDto {
    idOriginal: number;
    artOriginal: Articulos;
}
