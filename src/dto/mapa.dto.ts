export class MapaDto {
    MX: number;
    AR: number;
    BR: number;
    ES: number;
    FR: number;
    CL: number;
    CO: number;
    US: number;
    VE: number;
}
