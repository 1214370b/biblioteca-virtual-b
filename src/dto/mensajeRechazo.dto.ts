import { Usuarios } from 'src/entities/usuarios.entity';

export class MensajeRechazoDto {
  readonly receptorId: number;
  readonly emisorId: number;
  readonly cuerpo: string;
}
