import { Categorias } from '../entities/categorias.entity';
export class ArticulosDto {
  readonly usuarioid: number;
  readonly titulo: string;
  readonly categoriaid: Categorias;
  readonly fecha: string;
  readonly imagen: string;
  readonly cuerpo: string;
  readonly fecha_de_registro: string;
}
