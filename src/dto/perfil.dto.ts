export class PerfilDto {
    readonly Nombre: string;
    readonly Apellido: string;
    readonly correo: string;
    readonly fecha_de_nacimiento: string;
    readonly nacionalidad: string;
}