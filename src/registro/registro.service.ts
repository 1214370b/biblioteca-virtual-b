import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { Repository } from 'typeorm';
import { RegisterDto } from '../dto/register';
import { Usuarios } from '../entities/usuarios.entity';

//Registro de usuarios 
@Injectable()
export class RegistersService {

    constructor(
    @InjectRepository(Usuarios)
    private readonly registerRepository: Repository<Usuarios>,){}

    async getAll() {
        return await this.registerRepository.find();
    }

    async Register(registroNuevo: RegisterDto): Promise<Usuarios>{
        const nuevo = new Usuarios();
        nuevo.Nombre = registroNuevo.Nombre;
        nuevo.Apellido = registroNuevo.Apellido;
        nuevo.nacionalidad = registroNuevo.nacionalidad;
        nuevo.contrasena = registroNuevo.contrasena;
        nuevo.fecha_de_nacimiento = registroNuevo.fecha_de_nacimiento;
        nuevo.correo = registroNuevo.correo;
        nuevo.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString();
        nuevo.rol = registroNuevo.rol;

        return this.registerRepository.save(nuevo);
    }
}
