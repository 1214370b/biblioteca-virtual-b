import { Controller,Post,Get,Res,Body,HttpStatus } from '@nestjs/common';
import {RegisterDto} from '../dto/register';
import { RegistersService } from '../registro/registro.service'

//Registro de usuarios 
@Controller('registro')
export class RegistrationController {
    constructor(private registerService: RegistersService){
    }

    //crear usuario "registro"
    @Post()
    create (@Body() registerDo: RegisterDto, @Res() response){
        this.registerService.Register(registerDo).then( registro => {
            response.status(HttpStatus.CREATED).json(registro);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({registro: "error en tu registro"});});
    }

    /**
     * 
     * ejemplo de POST
     * 
     * {
        "Nombre": "postman",
        "Apellido": "ApellidoP",
        "nacionalidad": "MX",
        "contrasena": "cont1234",
        "fecha_de_nacimiento": "01/01/2020",
        "correo": "user@mail.com"
        }
     */

    //mostrar el lista de registro
    @Get()
    getAll(@Res() response){
        this.registerService.getAll().then(registrosList => {
            response.status(HttpStatus.OK).json(registrosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de registro'});
        })
    }

}
