import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ArticulosGuardados } from "./articulos-guardados.entity";
import { Articulos } from "./articulos.entity";
import { MaterialGuardado } from "./material-guardado.entity";
import { Mensajes } from "./mensajes.entity";
import { PeticionesDeAutor } from "./peticiones-de-autor.entity";


    export enum Rol {
        USUARIO = "usuario",
        AUTOR = "autor",
        ADMIN = "admin"
    }

    @Entity()
    export class Usuarios {

        @PrimaryGeneratedColumn()
        id: number;

        @Column()
        Nombre: string;

        @Column()
        Apellido: string;

        @Column({unique: true})
        correo: string;

        @Column()
        contrasena: string;

        @Column()
        fecha_de_nacimiento: string;

        @Column()
        nacionalidad: string;

        @Column({nullable: true})
        cv: string;

        @Column({default: true})
        activo: boolean;

        @Column()
        fecha_de_registro: string;

        @Column({
            type: "enum",
            enum: Rol,
            default: Rol.USUARIO
        })
        rol: Rol;

        //Relaciones con la tabla Usuarios
        @OneToMany( () => Articulos, articulos => articulos.autor)
        articulos: Articulos[];

        @OneToMany( () => PeticionesDeAutor, peticiones_de_autor => peticiones_de_autor.usuario )
        peticiones_de_autor: PeticionesDeAutor[];

        @OneToMany( () => ArticulosGuardados, articulos_guardados => articulos_guardados.usuario )
        articulos_guardados: ArticulosGuardados[];

        @OneToMany( () => MaterialGuardado, material_guardado => material_guardado.usuario )
        material_guardado: MaterialGuardado[];
        
        @OneToMany( () => Mensajes, mensajes => mensajes.emisor )
        mensajesEnv: Mensajes[];
        
        @OneToMany( () => Mensajes, mensajes => mensajes.receptor )
        mensajesRec: Mensajes[];

}

