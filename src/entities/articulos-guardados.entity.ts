import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Articulos } from "./articulos.entity";
import { Usuarios } from "./usuarios.entity";

@Entity()
export class ArticulosGuardados {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fecha_de_registro: string;

    @Column({default: true})
    activo: boolean;

    @ManyToOne(() => Usuarios, usuarios => usuarios.articulos_guardados)
    usuario: Usuarios;

    @ManyToOne(() => Articulos, articulos => articulos.articulos_guardados)
    articulo: Articulos;


}
