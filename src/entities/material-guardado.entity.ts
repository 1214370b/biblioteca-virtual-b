import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Material } from "./material.entity";
import { Usuarios } from "./usuarios.entity";

@Entity()
export class MaterialGuardado {
    
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fecha_de_registro: string;

    @Column({default: true})
    activo: boolean;

    @ManyToOne(() => Usuarios, usuarios => usuarios.articulos_guardados)
    usuario: Usuarios;

    @ManyToOne(() => Material, material => material.material_guardado)
    material: Material;

}
