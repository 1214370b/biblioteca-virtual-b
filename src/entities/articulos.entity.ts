import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ArticulosGuardados } from "./articulos-guardados.entity";
import { PeticionesEdicion } from "./peticiones-edicion.entity";
import { Usuarios } from "./usuarios.entity";
import { Categorias } from '../entities/categorias.entity'

export enum Estatus {
    //enum de estatus
    PENDIENTE = "pendiente",
    ACEPTADO = "aceptado",
    RECHAZADO = "rechazado"

}

    @Entity()
    export class Articulos {

        @PrimaryGeneratedColumn()
        id: number;

        @Column()
        titulo: string;

        @Column()
        imagen: string;

        @Column()
        cuerpo: string;

        @Column({nullable: true})
        etiquetas: string;

        @Column({default: true})
        activo: boolean;
        
        @Column()
        fecha_de_registro: string;

        @Column({
            type: "enum",
            enum: Estatus,
            default: Estatus.PENDIENTE
        })
        estatus: Estatus;

        //Relaciones hacia Usuarios y articulos_guardados
        @ManyToOne(() => Usuarios, usuarios => usuarios.articulos)
        autor: Usuarios;

        @ManyToOne(() => Categorias, categorias => categorias.nombre)
        categoria: Categorias;
        
        @OneToMany( () => ArticulosGuardados, articulos_guardados => articulos_guardados.articulo )
        articulos_guardados: ArticulosGuardados[];

        @OneToMany( () => PeticionesEdicion, peticiones_edicion => peticiones_edicion.articulo_original )
        peticiones_edicion: PeticionesEdicion[];
    }

