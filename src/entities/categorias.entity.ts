import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Categorias {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    fecha_de_registro: string;

    @Column({default: true})
    activo: boolean;
}