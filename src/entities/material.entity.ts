import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { MaterialGuardado } from "./material-guardado.entity";
import { Categorias } from './categorias.entity'

    @Entity()
    export class Material {

        @PrimaryGeneratedColumn()
        id: number;

        @Column()
        titulo: string;

        @Column()
        autor_del_doc: string;

        @Column()
        fecha: string;

        @Column({
            type: "mediumblob"   //max 2MB
        })
        archivo: string;

        @Column()
        descripcion: string;

        @Column({nullable: true})
        etiquetas: string;

        @Column({default: true})
        activo: boolean;

        @Column()
        fecha_de_registro: string;

        @OneToMany( () => MaterialGuardado, material_guardado => material_guardado.material)
        material_guardado: MaterialGuardado[];

        @ManyToOne(() => Categorias, categorias => categorias.nombre)
        categoria: Categorias;


    }

