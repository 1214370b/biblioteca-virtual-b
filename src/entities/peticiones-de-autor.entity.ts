import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Usuarios } from "./usuarios.entity";

@Entity()
export class PeticionesDeAutor {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Usuarios, usuarios => usuarios.peticiones_de_autor)
    usuario: Usuarios

    @Column()
    num_de_peticiones: number;

    @Column({default: false})   //cada nueva peticion entra como no revisada
    revisada: boolean;

    @Column()
    fecha_de_registro: string;

    @Column({default: true})
    activo: boolean;
}
