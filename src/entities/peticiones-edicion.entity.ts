import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Articulos } from "./articulos.entity";

@Entity()
export class PeticionesEdicion {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    titulo_nuevo: string;

    @Column()
    cuerpo_nuevo: string;

    @Column()
    imagen_nueva: string;

    @Column({default: false})
    revisada: boolean;

    @Column()
    fecha_de_registro: string;

    @Column({default: true})
    activo: boolean;

    @ManyToOne(() => Articulos, articulo => articulo.peticiones_edicion)
    articulo_original: Articulos;
}
