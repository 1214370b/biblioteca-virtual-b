import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Usuarios } from "./usuarios.entity";

@Entity()
export class Mensajes {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    cuerpo: string;

    @Column({default: false})
    leido: boolean;

    @Column()
    fecha_de_registro: string;

    @Column({default: true})
    activo: boolean;
    
    //Esta relacion genera unas tablas de union, mensaje_receptor_usuarios y mensaje_emisor_usuarios
    //pendiente de revision 
    /*
    @ManyToMany( () => Usuarios)
    @JoinColumn()
    emisor: Usuarios[];

    @ManyToMany( () => Usuarios)
    @JoinColumn()
    receptor: Usuarios[];
    */

    @ManyToOne( () => Usuarios, usuario => usuario.mensajesEnv )
    emisor: Usuarios;
    @ManyToOne( () => Usuarios, usuario => usuario.mensajesRec )
    receptor: Usuarios;
    
}
