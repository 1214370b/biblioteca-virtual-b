import { Controller, Body, Post,HttpStatus, Res} from '@nestjs/common';
import { LoginService} from '../login/login.service'
import { LoginDto } from '../dto/login'

//Inicio de sesion 
@Controller('login')
export class LoginController {
    constructor(private loginService:LoginService){
    }

    //iniciar sesion y token
    @Post()
    async Login(@Body() loginDo : LoginDto,@Res() response){
        this.loginService.Login(loginDo).then( login => {
            response.status(HttpStatus.OK).json(login);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({login: "Correo electrónico y/o contraseña no válidos. Por favor, inténtalo de nuevo"});
      });
   }
}