import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {LoginDto} from '../dto/login'
import { Usuarios } from '../entities/usuarios.entity';
import { JwtService } from '@nestjs/jwt';

//Inicio de sesion 
@Injectable()
export class LoginService {

    constructor(
    @InjectRepository(Usuarios)
    private readonly loginRepository: Repository<Usuarios>,
    private jwtService: JwtService){}


    //iniciar sesion y token 
    async Login( iniciarsesion: LoginDto ): Promise<any> {
        const user = await this.loginRepository.findOne(iniciarsesion);
        const payload = { correo: user.correo, contrasena: user.contrasena};
        return {access_token: this.jwtService.sign(payload)};
    }      
}
