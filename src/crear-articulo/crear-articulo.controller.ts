import { Controller, Post, Get, Res, Body, HttpStatus} from '@nestjs/common';
import { CrearArticuloService } from '../crear-articulo/crear-articulo.service'
import { ArticulosDto } from '../dto/articulos.dto';

@Controller('crear-articulo')
export class CrearArticuloController {
    constructor(private crearArticuloService: CrearArticuloService){
    }

    //crear post "Articulos"
    @Post()
    create (@Body() registerDo: ArticulosDto, @Res() response){
        this.crearArticuloService.crearArticulo(registerDo).then( registroArticulo => {
            response.status(HttpStatus.CREATED).json(registroArticulo);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({registro: "error"});
      });
    }

    //mostrar Articulos
    @Get()
    getAll(@Res() response){
        this.crearArticuloService.ArticulosgetAll().then(articulosList => {
            response.status(HttpStatus.OK).json(articulosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de datos'});
        })
    }
}
