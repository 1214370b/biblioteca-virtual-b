import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Articulos } from '../entities/articulos.entity';
import { ArticulosDto } from '../dto/articulos.dto';
import { Repository } from 'typeorm';
import * as moment from 'moment';

@Injectable()
export class CrearArticuloService {
  constructor(
    @InjectRepository(Articulos)
    private readonly crarArticuloRepository: Repository<Articulos>,
  ) {}

  //registrar
  async crearArticulo(crearNuevoArticulo: ArticulosDto): Promise<Articulos> {
    const nuevoArticulo = new Articulos();
    nuevoArticulo.titulo = crearNuevoArticulo.titulo;
    nuevoArticulo.categoria = crearNuevoArticulo.categoriaid;
    nuevoArticulo.cuerpo = crearNuevoArticulo.cuerpo;
    nuevoArticulo.imagen = crearNuevoArticulo.imagen;
    nuevoArticulo.fecha_de_registro = moment()
      .format('DD[/]MM[/]YYYY')
      .toString();
    return this.crarArticuloRepository.save(nuevoArticulo);
  }

  //lista de los articulos
  async ArticulosgetAll() {
    return await this.crarArticuloRepository.find();
  }
}
