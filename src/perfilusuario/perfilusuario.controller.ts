import { Controller, Get, HttpStatus, Param, Res } from '@nestjs/common';
import { response } from 'express';
import { PerfilusuarioService } from './perfilusuario.service'; 

@Controller('usuario')
export class PerfilusuarioController {
  constructor(private perfilusuarioService: PerfilusuarioService) {}

  @Get()
  getAll(@Res() response) {
    this.perfilusuarioService
      .getAll()
      .then((pusuarioList) => {
        response.status(HttpStatus.OK).json(pusuarioList);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de usuarios' });
      });
  }

  @Get(':id')
  get(@Res() response, @Param('id') id) {
    this.perfilusuarioService
      .get(id)
      .then((perfilid) => {
        response.status(HttpStatus.OK).json(perfilid);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de registro' });
      });
  }
}
