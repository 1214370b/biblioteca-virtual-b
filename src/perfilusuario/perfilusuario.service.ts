import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { Usuarios } from 'src/entities/usuarios.entity';
import { Repository } from 'typeorm';
import { PerfilusuarioDto } from 'src/dto/PerfilUsuario.dto';

@Injectable()
export class PerfilusuarioService {
  constructor(
    @InjectRepository(Usuarios)
    private readonly perfilRepository: Repository<Usuarios>,
  ) {}

  async getAll() {
    return await this.perfilRepository.find({ where: { rol: 'usuario' } });
  }

  async get(id: number): Promise<Usuarios> {
    if (!id) {
      throw new BadRequestException('el id debe ser mandado');
    }

    return await this.perfilRepository.findOne(id);
  }
}
