import { Body, Controller, Get, HttpStatus, Param, Put, Res } from '@nestjs/common';
import { MensajeDto } from 'src/dto/mensaje.dto';
import { GestionArticulosService } from './gestion-articulos.service';

@Controller('gestion-articulos')
export class GestionArticulosController {
    constructor(private gestionArticulosService: GestionArticulosService){}

    
    @Get()
    getAllArticulos(@Res() response){
        this.gestionArticulosService.getArticulos()
        .then(listArticulos => {
            response.status(HttpStatus.OK).json(listArticulos);
        })
        .catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de datos'});
        })
    }

    @Get("pendientes")
    getPendientes(@Res() response){
        this.gestionArticulosService.getArticulosPendientes()
        .then(listArticulos => {
            response.status(HttpStatus.OK).json(listArticulos);
        })
        .catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de datos'});
        })
    }
    @Get("eliminados")
    getEliminados(@Res() response){
        this.gestionArticulosService.getArticulosEliminados()
        .then(listArticulos => {
            response.status(HttpStatus.OK).json(listArticulos);
        })
        .catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de datos'});
        })
    }
    @Put("aceptar/:id")
    aceptarArticulo(@Res() response, @Param("id") id: number){
        this.gestionArticulosService.aceptarArticulo(id)
        .then(
            res => {
                response.status(HttpStatus.OK).json(res);
            }
        )
        .catch(
            () => { response.status(HttpStatus.FORBIDDEN).json({mensaje: "no se pudo aceptar el articulo"})}
        )
    }
    @Put("rechazar/:id")
    rechazarArticulo(@Res() response, @Param("id") id: number, @Body() mensaje: MensajeDto){
        this.gestionArticulosService.rechazarArticulo(id, mensaje)
        .then(
            res => {
                response.status(HttpStatus.OK).json(res);
            }
        )
        .catch(
            () => { response.status(HttpStatus.FORBIDDEN).json({mensaje: "no se pudo rechazar el articulo"})}
        )
    }

    @Get("modificados")
    getModificados(@Res() response){
        this.gestionArticulosService.getModificados()
        .then(
            list => {
                response.status(HttpStatus.OK).json(list);
            }
        )
        .catch(
            () => {response.status(HttpStatus.FORBIDDEN).json({mensaje: "no se pudo obtener la lista de modificados"}) }
        )
    }
    @Get("modificados/:id")
    comparar(@Res() response, @Param("id") id: number){
        this.gestionArticulosService.compararOriginal(id)
        .then(
            res => {
                response.status(HttpStatus.OK).json(res)
            }
        )
        .catch(
            () => {response.status(HttpStatus.NO_CONTENT).json({mensaje: "no se pudo obtener el elemento modificado"}) }

        )
    }
    
    @Put("modificados/aceptar/:id")
    aceptarCambios(@Res() response, @Param("id") id: number){
        this.gestionArticulosService.aceptarCambios(id)
        .then(
            res => {
                response.status(HttpStatus.OK).json(res)
            }
        )
        .catch(
            () => {response.status(HttpStatus.OK).json({mensaje: "no se pudo obtener el elemento modificado"}) }
        )
    }

    @Put("modificados/rechazar/:id")
    rechazarCambios(@Res() response, @Param("id") id: number, @Body() mensaje: MensajeDto){
        this.gestionArticulosService.rechazarCambios(id, mensaje)
        .then(
            res => {
                response.status(HttpStatus.OK).json(res)
            }
        )
        .catch(
            () => {response.status(HttpStatus.OK).json({mensaje: "no se pudo obtener el elemento modificado"}) }
        )
    }
}
