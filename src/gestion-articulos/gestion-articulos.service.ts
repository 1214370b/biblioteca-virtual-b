import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { authorize } from 'passport';
import { join } from 'path/posix';
import { MensajeDto } from 'src/dto/mensaje.dto';
import { Articulos, Estatus } from 'src/entities/articulos.entity';
import { Mensajes } from 'src/entities/mensajes.entity';
import { PeticionesEdicion } from 'src/entities/peticiones-edicion.entity';
import { Usuarios } from 'src/entities/usuarios.entity';
import { JoinColumn, Repository } from 'typeorm';

@Injectable()
export class GestionArticulosService {
    constructor(
        @InjectRepository(Articulos)
        private readonly articulosRepository: Repository<Articulos>,
        @InjectRepository(Mensajes)
        private readonly mensajesRepository: Repository<Mensajes>,
        @InjectRepository(Usuarios)
        private readonly usuariosRepository: Repository<Usuarios>,
        @InjectRepository(PeticionesEdicion)
        private readonly peticionesEdicionRepository: Repository<PeticionesEdicion>,
    ){}

    async getArticulos(){
        return await this.articulosRepository.find( {where:{activo: true}, relations: ['autor'] } );
    }
    async getArticulosPendientes(){
        return await this.articulosRepository.find( {where:{estatus: "pendiente"}, relations: ['autor'] } )
    }
    async getArticulosEliminados(){
        return await this.articulosRepository.find({where:{activo: false}})
    }

    async aceptarArticulo(idA: number){
        return await this.articulosRepository.update({id: idA},{estatus: Estatus.ACEPTADO})
    }
    async rechazarArticulo(idA: number, mensaje: MensajeDto){
        //se debe mandar mensaje
        const nuevoM = new Mensajes();

        const emisor = await this.usuariosRepository.findOne(mensaje.emisorId);
        const receptor = await this.usuariosRepository.findOne(mensaje.receptorId);

        nuevoM.emisor = emisor;
        nuevoM.receptor = receptor;
        nuevoM.cuerpo = mensaje.cuerpo;
        nuevoM.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString();

        await this.mensajesRepository.save(nuevoM);

        return await this.articulosRepository.update({id: idA},{estatus: Estatus.RECHAZADO})
    }

    /*
     Gestion de los articulo que fueron modificados por el usuario
     */

     async getModificados(){
        return await this.peticionesEdicionRepository.find( { where: {revisada: false}, relations: ['articulo_original'] } );
     }

     async compararOriginal(idM: number){
        return await this.peticionesEdicionRepository.findOne(
            {
                where: {id: idM}, 
                relations: ['articulo_original', 'articulo_original.autor'] 
            }
        )
     }

     async aceptarCambios(idM: number){
        const modificado = await this.peticionesEdicionRepository.findOne(
            { 
                where: {id: idM},
                relations: ['articulo_original'] 
            }
        );

        const original = modificado.articulo_original;

        await this.articulosRepository.update(
            {id: original.id},
            {
                titulo: modificado.titulo_nuevo,
                cuerpo: modificado.cuerpo_nuevo,
                imagen: modificado.imagen_nueva
            }
        );
        return await this.peticionesEdicionRepository.update({id: idM},{revisada: true})
     }

     async rechazarCambios(idM: number, mensaje: MensajeDto){
        const nuevoM = new Mensajes();

        const emisor = await this.usuariosRepository.findOne(mensaje.emisorId);
        const receptor = await this.usuariosRepository.findOne(mensaje.receptorId);

        nuevoM.emisor = emisor;
        nuevoM.receptor = receptor;
        nuevoM.cuerpo = mensaje.cuerpo;
        nuevoM.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString();

        await this.peticionesEdicionRepository.update({id: idM},{revisada: true})
        return await this.mensajesRepository.save(nuevoM);
     }

}
