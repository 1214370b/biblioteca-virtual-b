import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Articulos } from '../entities/articulos.entity';
import { ArticulosDto } from '../dto/articulos.dto';
import { Repository } from 'typeorm';
import * as moment from 'moment';
import { Usuarios } from '../entities/usuarios.entity';
import { ArticulosGuardados } from '../entities/articulos-guardados.entity';
import { ArticulosGuardadosDto } from '../dto/articulos-guardados.dto';
import { Categorias } from '../entities/categorias.entity';

@Injectable()
export class ArticuloService {

    constructor(
        @InjectRepository(Articulos)
        private readonly ArticuloRepository: Repository<Articulos>,
        @InjectRepository(Usuarios)
        private readonly usuariosRepository: Repository<Usuarios>,
        @InjectRepository(ArticulosGuardados)
        private readonly guardarArticuloRepository: Repository<ArticulosGuardados>,
        @InjectRepository(Categorias)
        private readonly categoriasRepository: Repository<Categorias>,
        ){}
    
        //crear articulo 
        async crearArticulo(crearNuevoArticulo: ArticulosDto){
            const nuevoArticulo = new Articulos();
            const usuarioid = await this.usuariosRepository.findOne(crearNuevoArticulo.usuarioid);
            const categoriaid = await this.categoriasRepository.findOne(crearNuevoArticulo.categoriaid);

            nuevoArticulo.autor = usuarioid
            nuevoArticulo.categoria = categoriaid
            nuevoArticulo.titulo= crearNuevoArticulo.titulo
            nuevoArticulo.cuerpo = crearNuevoArticulo.cuerpo
            nuevoArticulo.imagen = crearNuevoArticulo.imagen
            nuevoArticulo.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString()   

            return await this.ArticuloRepository.save(nuevoArticulo)
        }

        //lista de los articulos
        async ArticulosgetAll() {
            return await this.ArticuloRepository.find();
        }

        //guardar articulos
        async guardararticulo(guardardo: ArticulosGuardadosDto){
            const guardarArticulo= new  ArticulosGuardados();
            const usuarioid = await this.usuariosRepository.findOne(guardardo.usuarid);
            const articuloid = await this.ArticuloRepository.findOne(guardardo.articuloid);

            guardarArticulo.usuario = usuarioid
            guardarArticulo.articulo = articuloid
            guardarArticulo.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString();

            return await this.guardarArticuloRepository.save(guardarArticulo)
        }
}
