import { Controller, Post, Get, Res, Body, HttpStatus} from '@nestjs/common';
import { ArticuloService } from '../articulos/articulos.service'
import { ArticulosDto } from '../dto/articulos.dto';
import { ArticulosGuardadosDto } from '../dto/articulos-guardados.dto'

@Controller('crear-articulo')
export class ArticuloController {
    constructor(private ArticuloService: ArticuloService){
    }

    //crear post "Articulos"
    @Post()
    createArticulos (@Body() registerDo: ArticulosDto, @Res() response){
        this.ArticuloService.crearArticulo(registerDo).then( registroArticulo => {
            response.status(HttpStatus.CREATED).json(registroArticulo);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({registro: "error"});
      });
    }

    //lista de Articulos
    @Get()
    getAll(@Res() response){
        this.ArticuloService.ArticulosgetAll().then(articulosList => {
            response.status(HttpStatus.OK).json(articulosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de datos'});
        })
    }

    //guardar articulo
    @Post('guardar')
    guardar(@Body() guardarDo: ArticulosGuardadosDto,@Res() response){
        this.ArticuloService.guardararticulo(guardarDo).then( guardararticulo => {
            response.status(HttpStatus.CREATED).json(guardararticulo);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({guardararticulo: "inténtalo de nuevo"});
      });
   }
}
