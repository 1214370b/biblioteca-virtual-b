/* eslint-disable prettier/prettier */
import { Controller, Get, HttpStatus, Res,Put,Param, Body } from '@nestjs/common';
import { response } from 'express';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { SolicitudesService } from './solicitudes.service';
import { MensajeRechazoDto } from 'src/dto/mensajeRechazo.dto';

@Controller('solicitudes')
export class SolicitudesController {
  constructor(private SolicitudesService: SolicitudesService) {}

  @Get()
  getsolicitudes(@Res() response) {
    this.SolicitudesService.getsolicitudes()
      .then((listSolicitudes) => {
        response.status(HttpStatus.OK).json(listSolicitudes);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de Solicitudes' });
      });
  }


  @Get(':id')
  get(@Res() response, @Param('id') id) {
    this.SolicitudesService
      .get(id)
      .then((solicitudesid) => {
        response.status(HttpStatus.OK).json(solicitudesid);
      })
      .catch(() => {
        response
          .status(HttpStatus.FORBIDDEN)
          .json({ mensaje: 'error en la obtencion de id' });
      });
  }


  @Put("aceptar/:id")
    aceptarsolicitud(@Res() response, @Param("id") id){
        this.SolicitudesService.aceptarsolicitud(id).then(
            res => {
                response.status(HttpStatus.OK).json(res);
            }
        ).catch(
            () => { response.status(HttpStatus.FORBIDDEN).json({mensaje: "invalido"})}
        )
    }
    
@Put("Rechazar/:id")
    rechazarsolicitud(@Res() response, @Param("id") id, @Body() mensaje:MensajeRechazoDto){
    this.SolicitudesService.rechazarsolicitud(id,mensaje).then(
        res => {
            response.status(HttpStatus.OK).json(res);
        }
    ).catch(
        () => { response.status(HttpStatus.FORBIDDEN).json({mensaje: "no se pudo aceptar la solicitud"})}
    )
}
}






    


    function aceptarsolicitud() {
        throw new Error('Function not implemented.');
    }

