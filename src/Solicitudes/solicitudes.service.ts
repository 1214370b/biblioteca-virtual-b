/* eslint-disable @typescript-eslint/adjacent-overload-signatures */
/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PeticionesDeAutor } from '../entities/peticiones-de-autor.entity';
import { Repository } from 'typeorm';
import { Rol, Usuarios } from 'src/entities/usuarios.entity';
import { MensajeRechazoDto } from 'src/dto/mensajeRechazo.dto';
import { Mensajes } from 'src/entities/mensajes.entity';
import * as moment from 'moment';
import { Articulos } from 'src/entities/articulos.entity';


@Injectable()
export class SolicitudesService {
   

  constructor(
    @InjectRepository(PeticionesDeAutor)
    private readonly PeticionesDeAutorRepository: Repository<PeticionesDeAutor>,
    @InjectRepository(Usuarios)
    private readonly usuariosRepository: Repository<Usuarios>,
    @InjectRepository(Mensajes)
    private readonly mensajesRepository:Repository<Mensajes>
  ) {}

  async get(id: number): Promise<PeticionesDeAutor> {
    if (!id) {
      throw new BadRequestException('el id debe ser mandado');
    }

    return await this.PeticionesDeAutorRepository.findOne(id);
  }
  
  async getsolicitudes() {
    return await this.PeticionesDeAutorRepository.find();
  }

  async aceptarsolicitud(idA: number){
    return await this.usuariosRepository.update({id: idA},{rol: Rol.AUTOR})
}

async solicitudid(idA: number){
  return await this.usuariosRepository.update({id: idA},{rol: Rol.AUTOR})
}


async rechazarsolicitud(idA: number, mensajeRechazo: MensajeRechazoDto){
  //se debe mandar mensaje
  const nuevoM = new Mensajes();

  const emisor = await this.usuariosRepository.findOne(mensajeRechazo.emisorId);
  const receptor = await this.usuariosRepository.findOne(mensajeRechazo.receptorId);

  nuevoM.emisor = emisor;
  nuevoM.receptor = receptor;
  nuevoM.cuerpo = mensajeRechazo.cuerpo;
  nuevoM.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString();

  return await this.mensajesRepository.save(nuevoM);

  
}


}

