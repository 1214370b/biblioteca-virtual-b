import { Injectable, Put, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Material } from '../entities/material.entity'
import { Repository } from 'typeorm';
import { Usuarios } from '../entities/usuarios.entity';
import { MaterialGuardado } from '../entities/material-guardado.entity'
import { MaterialGuardadoDto } from '../dto/material-guardado.dto';
import * as moment from 'moment';


//material didactico
@Injectable()
export class MaterialService {
    constructor(
        @InjectRepository(Material)
        private readonly listamaterialRepository: Repository<Material>,
        @InjectRepository(Usuarios)
        private readonly usuariosRepository: Repository<Usuarios>,
        @InjectRepository(MaterialGuardado)
        private readonly materialRepository: Repository<MaterialGuardado>,){}
    
        //lista de material didactico/catalogo 
        async materialgetAll() {
            return await this.listamaterialRepository.find();
        }

        //guardar material didactico/catalogo 
        async guardarmaterial(guardardo: MaterialGuardadoDto){
            const nuevoMaterial = new MaterialGuardado();
            const usuarioid = await this.usuariosRepository.findOne(guardardo.usuarioid);
            const materialid = await this.listamaterialRepository.findOne(guardardo.materialid);

            nuevoMaterial.usuario = usuarioid
            nuevoMaterial.material = materialid
            nuevoMaterial.fecha_de_registro = moment().format("DD[/]MM[/]YYYY").toString();

            return await this.materialRepository.save(nuevoMaterial)
        }
        
}