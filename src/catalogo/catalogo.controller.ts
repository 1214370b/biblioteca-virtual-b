import { Controller, Get, HttpStatus, Res, Post, Put, Param, Body} from '@nestjs/common';
import { MaterialService } from './catalogo.service'
import { MaterialGuardadoDto } from '../dto/material-guardado.dto'
import { MaterialDto } from '../dto/material.dto';

//material didactico
@Controller('catalogo')
export class MaterialController {
    constructor(private materialService: MaterialService){}

    //lista de material didactico
    @Get()
    getMaterial(@Res() response) {
        this.materialService.materialgetAll().then((listMaterial) => {
            response.status(HttpStatus.OK).json(listMaterial);
            }).catch(() => {
                response.status(HttpStatus.FORBIDDEN).json({ mensaje: 'error en la obtencion de datos' });
      });
    }

    //guardar material didactico
    @Post()
    guardar(@Body() guardarDo: MaterialGuardadoDto,@Res() response){
        this.materialService.guardarmaterial(guardarDo).then( guardarMaterial => {
            response.status(HttpStatus.CREATED).json(guardarMaterial);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({guardarMaterial: "inténtalo de nuevo"});
      });
   }
}

